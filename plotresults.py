import sys
import json
import yaml
import tempfile
import numpy as np
import matplotlib.pyplot as plt
import os

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Plot optimized vs. real spectrum.')
    parser.add_argument('-i', '--input', required=True, help='json file with optimization results')
    parser.add_argument('-r', '--real', required=True, help='real supernova spectrum')
    parser.add_argument('-t', '--tardis', required=True, help='TARDIS yaml configuration files to be used for default values')
    args = parser.parse_args()
    results = json.loads(open(args.input, 'r').read())
    config = yaml.load(open(args.tardis, 'r').read())
    x = np.array(results['results'][0]['x'])
    init = [[7000.0, 15000.0], [4e42, 3e43]] + [[0.0, 1.0]] * 11
    for index, x_ in enumerate(x):
        if x_ < init[index][0]:
            x[index] = init[index][0]
        if x_ > init[index][1]:
            x[index] = init[index][1]
    abundances = x[2:] / x[2:].sum()
    config['model']['structure']['velocity']['start'] = "%f km/s" % x[0]
    config['supernova']['luminosity_requested'] = "%f erg/s" % x[1]
    config['model']['abundances']['O'] = float(abundances[0])
    config['model']['abundances']['Si'] = float(abundances[1])
    config['model']['abundances']['S'] = float(abundances[2])
    config['model']['abundances']['Ca'] = float(abundances[3])
    config['model']['abundances']['Ti'] = float(abundances[4])
    config['model']['abundances']['Fe'] = float(abundances[5])
    config['model']['abundances']['Co'] = float(abundances[6])
    config['model']['abundances']['Ni'] = float(abundances[7])
    config['model']['abundances']['Mg'] = float(abundances[8])
    config['model']['abundances']['Cr'] = float(abundances[9])
    config['model']['abundances']['C'] = float(abundances[10])
    (fd1, filename1) = tempfile.mkstemp()
    print filename1
    os.write(fd1, yaml.dump(config))
    os.close(fd1)
    (fd2, filename2) = tempfile.mkstemp()
    os.close(fd2)
    os.system("tardis %s %s > /dev/null 2>&1" % (filename1, filename2))
    real = []
    for line in open(args.real, 'r'):
        real.append([float(nr.strip()) for nr in line.strip().split()])
    real = sorted(real, key=lambda row: row[0])
    real = np.array(real)
    synthetic = []
    for line in open(filename2, 'r'):
        synthetic.append([float(nr.strip()) for nr in line.strip().split()])
    synthetic = sorted(synthetic, key=lambda row: row[0])
    synthetic = np.array(synthetic)
    plt.plot(real[:,0], real[:,1])
    plt.plot(synthetic[:,0], synthetic[:,1])
    plt.show()

    
    
    
