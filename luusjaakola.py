from abc import ABCMeta, abstractmethod
from dalek.parallel.launcher import FitterLauncher, fitter_worker
from dalek.fitter.base import SimpleRMSFitnessFunction, BaseOptimizer, BaseFitter
from dalek.parallel.parameter_collection import ParameterCollection
from tardis.io.config_reader import ConfigurationNameSpace, Configuration
from tardis.atomic import AtomData
import numpy as np
import yaml

# class BaseFitter(object):
#     """
#     """
#     def __init__(self, optimizer, remote_clients, fitness_function, atom_data, default_config, max_iterations=50, worker=fitter_worker):
#         self.max_iterations = max_iterations
#         self.default_config = default_config
#         self.launcher = FitterLauncher(remote_clients, fitness_function, atom_data, worker)
#         self.optimizer = optimizer

#     def evaluate_parameter_collection(self, parameter_collection):
#         config_dict_list = parameter_collection.to_config(self.default_config)
#         fitnesses_result = self.launcher.queue_parameter_set_list(config_dict_list)
#         fitnesses_result.wait()
#         parameter_collection['dalek.fitness'] = fitnesses_result.result
#         return parameter_collection

#     def run_single_fitter_iteration(self, parameter_collection):
#         evaluated_parameter_collection = self.evaluate_parameter_collection(parameter_collection)
#         new_parameter_collection = self.optimizer(evaluated_parameter_collection)
#         return new_parameter_collection

#     def run_fitter(self, initial_parameters):
#         current_parameters = initial_parameters
#         i = 0
#         while i <= self.max_iterations:
#             current_parameters = self.run_single_fitter_iteration(current_parameters)
#             self.current_parameters = self.evaluate_parameter_collection(current_parameters)
#             i += 1


# class BaseOptimizer(object):
#     __metaclass__ = ABCMeta

#     def __init__(self):
#         pass

#     @abstractmethod
#     def __call__(self, *args, **kwargs):
#         pass


# class BaseFitnessFunction(object):
#     __metaclass__ = ABCMeta

#     def __init__(self, *args, **kwargs):
#         pass

#     @abstractmethod
#     def __call__(self, *args, **kwargs):
#         raise NotImplementedError

        

class LuusJaakolaOptimizer(BaseOptimizer):
    def __init__(self, *args, **kwargs):
        self.lbound = np.array([7000.0, 4e42] + [0.0] * 11)
        self.ubound = np.array([15000.0, 3e43] + [1.0] * 11)
        self.bounds = np.array(zip(self.lbound, self.ubound))
        #self.x = np.array([np.random.uniform(low, high) for low, high in self.bounds])
        self.x = (self.lbound + self.ubound) * 0.5
        self.n = 36
        self.d = np.array(self.ubound - self.lbound) * 0.5
        self.columns = ['model.structure.velocity.item0',
                        'supernova.luminosity_requested',
                        'model.abundances.o',
                        'model.abundances.si',
                        'model.abundances.s',
                        'model.abundances.ca',
                        'model.abundances.ti',
                        'model.abundances.fe',
                        'model.abundances.co',
                        'model.abundances.ni',
                        'model.abundances.mg',
                        'model.abundances.cr',
                        'model.abundances.c']

    def violates_boundaries(self, x):
        return any(x < self.lbound) or any(x > self.ubound)

    def __call__(self, parameter_collection):
        self.ys = parameter_collection['dalek.fitness']
        self.x, self.y = min(zip(self.xs, self.ys), key=lambda pair: pair[1])
        self.d *= 0.95
        xs = []
        for _ in range(self.n):
            a = np.random.uniform(-self.d, self.d)
            x_ = self.x + a
            while self.violates_boundaries(x_):
                a = np.random.uniform(-self.d, self.d)
                x_ = self.x + a
            xs.append(x_)
        self.xs = np.array(xs)
        xs = np.array(xs)
        return ParameterCollection(xs, columns=self.columns)
        
    def init_parameter_collection(self):
        xs = []
        for _ in range(self.n):
            a = np.random.uniform(-self.d, self.d)
            x_ = self.x + a
            while self.violates_boundaries(x_):
                a = np.random.uniform(-self.d, self.d)
                x_ = self.x + a
            xs.append(x_)
        self.xs = np.array(xs)
        xs = np.array(xs)
        return ParameterCollection(xs, columns=self.columns)
    

if __name__ == '__main__':
    from IPython.parallel import Client
    rc = Client()
    optimizer = LuusJaakolaOptimizer()
    data = np.loadtxt('origspec_2002bo_max_unreddened_lum.dat')
    observed_wavelength = data[:,0]
    observed_flux = data[:,1]
    function = SimpleRMSFitnessFunction(observed_wavelength, observed_flux)
    atom_data = AtomData.from_hdf5('kurucz_cd23_chianti_H_He.h5')
    #with open("tardis_02bo_kurucz.yml", 'r') as config:
    default_config = ConfigurationNameSpace.from_yaml("tardis_02bo_kurucz.yml")
    fitter = BaseFitter(optimizer, rc, function, atom_data, default_config, max_iterations=1)
    fitter.run_fitter(optimizer.init_parameter_collection())
    with open("output.yaml", 'w') as fd:
        yaml.dump(fitter.current_parameters.to_config(default_config), fd)
