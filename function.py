import numpy as np
import yaml
import os
import subprocess
import tempfile
from tardis import simulation
from tardis.model import Radial1DModel
from tardis.io import config_reader
import sys

def measure_fit(real, synthetic):
    """
    Measure Chi^2
    
    Arguments:
    ----------
    real -- real spectrum
    synthetic -- synthetic spectrum, x column must be in increasing order

    Returns:
    --------
    Chi^2 error
    """
    s_y = np.interp(real[:,0], synthetic[:,0], synthetic[:,1])
    r_y = real[:,1]
    return ((r_y - s_y) ** 2 / r_y).sum()

class SpectrumFit:
    def __init__(self, real_spectrum_file, yaml_file):
        self.real_spectrum_file = real_spectrum_file
        self.yaml_file = yaml_file
        self.d = 13
        self.init = [[7000.0, 15000.0], [4e42, 3e43]] + [[0.0, 1.0]] * 11
        self.name = "SpectrumFit"
        self.real = []
        with open(real_spectrum_file, 'r') as fd:
            for line in fd:
                self.real.append([float(nr.strip()) for nr in line.strip().split()])
        self.real = sorted(self.real, key=lambda row: row[0])
        self.real = np.array(self.real)
        with open(self.yaml_file, 'r') as fd:
            self.config = yaml.load(fd)

    def __call__(self, x):
        for index, x_ in enumerate(x):
            while x_ < self.init[index][0]:
                x[index] += self.init[index][1] - self.init[index][0]
            while x_ > self.init[index][1]:
                x[index] -= self.init[index][1] - self.init[index][0]
        velocity_start, luminosity_requested = x[:2]
        self.config['model']['structure']['velocity']['start'] = "%f km/s" % velocity_start
        self.config['supernova']['luminosity_requested'] = "%f erg/s" % luminosity_requested
        o, si, s, ca, ti, fe, co, ni, mg, cr, c = x[2:] / x[2:].sum()
        self.config['model']['abundances']['O'] = float(o)
        self.config['model']['abundances']['Si'] = float(si)
        self.config['model']['abundances']['S'] = float(s)
        self.config['model']['abundances']['Ca'] = float(ca)
        self.config['model']['abundances']['Ti'] = float(ti)
        self.config['model']['abundances']['Fe'] = float(fe)
        self.config['model']['abundances']['Co'] = float(co)
        self.config['model']['abundances']['Ni'] = float(ni)
        self.config['model']['abundances']['Mg'] = float(mg)
        self.config['model']['abundances']['Cr'] = float(cr)
        self.config['model']['abundances']['C'] = float(c)
	try:
            tardis_config = config_reader.TARDISConfiguration.from_config_dict(self.config)
            radial1d_model = Radial1DModel(tardis_config)
            simulation.run_radial1d(radial1d_model, None)
            (fd, filename) = tempfile.mkstemp()
            os.close(fd)
            if not np.all(radial1d_model.spectrum_virtual.luminosity_density_lambda.value == 0.0):
                radial1d_model.spectrum_virtual.to_ascii(filename)
            else:
                radial1d_model.spectrum.to_ascii(args.filename)
            synthetic = []
            with open(filename, 'r') as fd:
                for line in fd:
                    synthetic.append([float(nr.strip()) for nr in line.strip().split()])
            os.system("rm %s" % filename)
            synthetic = sorted(synthetic, key=lambda row: row[0])
            synthetic = np.array(synthetic)
            return measure_fit(self.real, synthetic)
        except:
            import random
            with open("exception%d.yaml" % random.randint(1, 1000), 'w') as fd:
                fd.write(yaml.dump(self.config))

function = SpectrumFit
