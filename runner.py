from swarmlab.swarm import Swarm
from swarmlab.config import Config
from swarmlab.problem import Problem

class PSOOptimizer(BaseOptimizer):
    def __init__(self):
        cfg = Config('topology.particle-type', 'bconstrict',
                     'function.problem-parameters.d', 13)
        self.swarm = Swarm(Problem, Config)
        self.swarm.reset()

    def __call__(self, param_collection):
        if np.any(param_collection.fitness == np.nan):
            raise ValueError
        self.swarm.update_particle_positions(xs, ys)
        for p, x in zip(parameter_collection, xs):
            p['model']['structure']['velocity']['start'] = "%f km/s" % x[0]
            p['supernova']['luminosity_requested'] = "%f erg/s" % x[1]
            map(lambda key, value: p['model']['abundances'].__setitem__(key, float(value)), 
                zip(['O', 'Si', 'S', 'Ca', 'Ti', 'Fe', 'Co', 'Ni', 'Mg', 'Cr', 'C'], xs[2:]))
        return param_collection
