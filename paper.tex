\documentclass[12pt]{article}
\usepackage{charter}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsfonts}
\usepackage{array}
\usepackage{url}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage[left=1in, right=2in]{geometry}
\usepackage[textwidth=1.7in]{todonotes}

\title{Fitting Supernova Spectra Using PSO}
\author{
  Vytautas Jan\v{c}auskas, Wolfgang Kerzendorf
}
\date{\today}

\begin{document}
\maketitle

\listoftodos

\section{Introduction}

\section{Similar Work}

M.R. Mokiem and A. de Koter et al. \cite{mokiem2005spectral} attempt to solve a similar problem using a genetic algorithm. They use PIKAIA developed by Paul Charbonneau and Barry Knapp \cite{charbonneau1995user}. For measuring spectrum fit they use reduced chi-squared. However they use different simulation algorithms for generating the spectra. J. A. Valenti and Nikolai Piskunov \cite{valenti1996spectroscopy} also try to solve a very similar problem of fitting synthetic star spectrum \todo{I don't understand how they do this}.

\section{Differential Evolution}

\section{Particle Swarm Optimization}

Particle Swarm Optimization is a global optimization metaheuristic designed for continuous problems. It was first proposed by James Kennedy and Russell C. Eberhart in 1995 \cite{kennedy95}. The idea is to have a swarm of particles (points in multi-dimensional space) each having some other particles as neighbours and exchanging information to find optimal solutions. Particles move in the solution space of some function by adjusting their velocities to move towards the best solutions they found so far and towards the best solutions found by their neighbours. These two attractors are further randomly weighted to allow more diversity in the search process. The idea behind this algorithm are observations from societies acting in nature. For example one can imagine a flock of birds looking for food by flying towards other birds who are signaling a potential food source as well as by remembering where this particular bird itself has seen food before and scouting areas nearby. Parts of it can also be viewed as modelling the way we ourselves solve problems - by imitating people we know, who we see as particularly successful, but also by learning on our own. Thus problem solving being influenced by our own experience and by the experience of people we know to have solved similar problems particularly well. The original algorithm is not presented here since it is very rarely used today and we go straight to more modern implementations.

\subsection{Canonical Particle Swarm}

Proposed by Maurice Clerc et al. \cite{clerc02} is a variant of the original PSO algorithm. It guarantees convergence through the use of the constricting factor $\chi$. It also has the advantage of not having any parameters, except for $\phi_1$ and $\phi_2$ which represent the influence of the personal best solution and the best solution of particles neighbours on the trajectory of that particle. Both of these parameters are usually set to $2.05$ as per suggestion in the original paper. Moving the particle in solution space is done by adding the velocity vector to the old position vector as illustrated in (\ref{movement}) equation.

\begin{equation}
    \vec{x}_i \leftarrow \vec{x}_i + \vec{v}_i
    \label{movement}
\end{equation}

Updating velocity involves taking current velocity and adjusting it so that it will point the particle more in the direction of its personal best and the best of its most successful neighbour. It is layed out in (\ref{velocity}) formula.

\begin{equation}
    \vec{v}_i \leftarrow \chi \left(\vec{v}_i + \vec{\rho_1} \otimes (\vec{p}_i - \vec{x}_i) + \vec{\rho_2} \otimes (\vec{g}_i - \vec{x}_i) \right)
    \label{velocity}
\end{equation}

where

\begin{equation}
    \rho_i = \vec{U}(0, \phi_i)
\end{equation}

\begin{equation}
    \chi = \frac{2}{\phi - 2 + \sqrt{\phi^2 - 4 \phi}}
\end{equation}

and where $\phi = \phi_1 + \phi_2$ with $\phi_1$ and $\phi_2$ set to 2.05, $\vec{U}(a, b)$ is a vector of random numbers from the uniform distribution ranging from $a$ to $b$ in value. Here $\vec{p}_i$ is the best personal solution of particle $i$ and $\vec{g_i}$ is the solution found by a neighbour of particle $i$. Which particle is a neighbour of which other particle is set in advance.

\begin{algorithm}
  \begin{algorithmic}[1]
    \For{$i \leftarrow 1$ to $n$}
    \For{$j \leftarrow 1$ to $d$}
    \State{$x_{ij} \leftarrow U(a_j, b_j)$}
    \State{$v_{ij} \leftarrow 0$}
    \EndFor
    \EndFor
    \While{stopping criteria not met}
    \For{$i \leftarrow 1$ to $n$}
    \State{$\vec{v}_i \leftarrow \chi \left(\vec{v}_i + \vec{\rho_1} \otimes (\vec{p}_i - \vec{x}_i) + \vec{\rho_2} \otimes (\vec{g}_i - \vec{x}_i) \right)$}
    \State{$\vec{x}_i \leftarrow \vec{x}_i + \vec{v}_i$}
    \If{$f(\vec{x}_i) < f(\vec{p}_i)$}
    \State{$\vec{p}_i \leftarrow \vec{x}_i$}
    \EndIf
    \EndFor
    \EndWhile
  \end{algorithmic}
  \label{canonicalpso}
  \caption{Canonical PSO algorithm.}
\end{algorithm}

The canonical variant of the PSO algorithm is given in Algorithm \ref{canonicalpso} and can be explained in plain words as follows: for each particle with index $j$ from $n$ particles in the swarm, initialize the position vector $\vec{x}_j$ to random values from the range specific to function $f$ and initialize the velocity vector to the zero vector, for $k$ iterations (set by the user) update the position vector according to formula (\ref{movement}) and update velocity according to formula (\ref{velocity}), recording best positions found for each particle.

\subsection{Particle Swarm Topologies}

Particle swarm topology is a name used for the graph that has as it's vertices swarm particles and where edges represent neighbourhood relations of the swarm. That is if two particles are connected by an edge then they use each others personal best information in the velocity update equation (\ref{velocity}) which means that they directly influence each others movement. Popular topologies include a fully connected graph, particles connected in a ring with either two or more of the closest particles in that ring and a von Neumann neighbourhood which is simply a two dimensional grid where each particle is connected to the particles at the top, bottom, left and right. We refer to those topologies as \verb=full=, \verb=ring= and \verb=grid= respectively. It is important to note that particle topology is an algorithm parameter that is usually set during the set-up phase and does not change during the course of the algorithm, although variants of PSO using dynamic neighbourhoods also exist. Many studies in to the influence of PSO topologies to the performance of the algorithm when optimizing functions exist. They show that the choice of topology has a profound influence on the performance of PSO. For example of such work we refer the reader to work by James Kennedy and Rui Mendes \cite{kennedy02} or James Kennedy \cite{kennedy1999small}.

\section{Fitting Supernova Spectra}

\section{Experimental Setup}

For the experimental part of this paper we evaluated the performance of various global optimization algorithms for fitting simulated supernova spectra to observed supernova spectra. Supernova explosion simulation is performed using the TARDIS library/system written in Python by the TARDIS development team \cite{tardis}. We used five spectra for the experiments. We tried to fit each of the five spectra with each of the methods used. The methods we used were: Neddler-Mead, Random Search, Differential Evolution, and Cannonical Particle Swarm with fully-connected, ring and grid topologies. Neddler-Mead and Random Search were used in case the problem turns out straight-forward enough to be solved using those simple methods. Differential Evolution and Particle Swarm Optimization were chosen because they seem to be among the main contenders in the field of real-valued global optimization meta-heuristics. With every method we used the same way of handling the linear constraint of the problem. A simple static penalty method was used. This is because there is only one, simple constraint so a more complicated penalty function does not seem justified. Furthermore the variables are scaled to add up to one by the fitness function itself. We also tried omitting the penalty function for each of the methods. The final fitness function in this case is given in equation (\ref{penalty}) below. \todo{Determine $C$ empirically.} With $C$ determined empirically.

\begin{equation}
  \hat{f}(\vec{x}) = f(\vec{x}) + C|h(\vec{x})|^2
  \label{penalty}
\end{equation}

\subsection{Optimization Algorithms}

We use four different optimization algorithms for comparison. These are: Nelder-Mead, Luus-Jaakola, Differential Evolution and Particle Swarm Optimization with three different topologies. Differential Evolution and Particle Swarm Optimization are described in their separate sections (being more involved methods) while Nelder-Mead and Luus-Jaakola as used for this paper are described here. We only provide parameter values as they were used for PSO and DE here.

Nelder-Mead (also sometimes known as the amoeba method) is a simple hill-climbing algorithm that does not rely on gradient information. We used a variant described below.

\begin{algorithm}
  \begin{algorithmic}[1]
    \State{state}
  \end{algorithmic}
  \label{neldermead}
  \caption{Nelder-Mead Algorithm}
\end{algorithm}

We used a very simple random search method. The particular variant of random search we used was first described by Rein Luus and T.H.I. Jaakola \cite{luus1973optimization}. The algorithm is described below.

\begin{algorithm}
  \begin{algorithmic}[1]
    \State{$\vec{x} \leftarrow \vec{U}\left(\vec{b}_l, \vec{b}_u\right)$}
    \While{stopping criteria not met}
    \For{$i \leftarrow \{1, \ldots, n\}$}
    \Repeat
    \State{$\vec{a}_i \leftarrow \vec{U}\left({-\vec{d}, \vec{d}}\right)$}
    \State{$\vec{x}_i \leftarrow \vec{x} + \vec{a}_i$}
    \Until{$\vec{x}_i$ satisfies boundary constraints}
    \EndFor
    \State{$\vec{x} \leftarrow \min_{\vec{x}_i}\{f(\vec{x}_1), \ldots, f(\vec{x}_n)\}$}
    \State{$\vec{d} \leftarrow 0.95 \vec{d}$}
    \EndWhile
  \end{algorithmic}
  \label{luusjaakola}
  \caption{Luus-Jaakola Algorithm}
\end{algorithm}

The parameters $c_1 = c_2 = 2.05$ for the PSO algorithm. The number of particles for PSO and individuals for Differential Evolution was set to $n = 36$ due to recommendations often found in the literature to use 3-5 as many individuals as there are dimensions in the problem \todo{Cite sources for these recommendations.}. Also the \verb=grid= topology requires a number of particles with an integral root.

We used the following stopping criterium --- stop the algorithm if for a given number of iterations $I$ the solution does not improve by more than $\epsilon$ with $I = 50$ and $\epsilon = 0.1$ \todo{The values for $I$ and $\epsilon$ I made up.} for all algorithms. Using this stopping criterium instead of a fixed number of function evaluations will allow us to also measure algorithm convergence speed. All other things being equal an algorithm that finds a good solution faster is better.

\subsection{Test Functions}

Apart from the main experiment with the synthetic spectrum fitting we also evaluated the algorithms using a set of problems commonly used for evaluating evolutionary computation and other optimization algorithms. For each problem and algorithm combination we performed 100 optimization runs using the exact same parameters as for the main experiment. After that the lowest scoring, highest scoring solutions and the median solution were recorded in to a table. The functions and their properties are summarized in Table \ref{testfunctions}.

\begin{equation}
  f(\vec{x}) = \sum_{i = 1}^k x_i^2
  \label{sphere}
\end{equation}

\begin{equation}
  \sum_{i = 1}^{n}\left(x_i^2 - 10 cos(2 \pi x_i) + 10\right)
  \label{rastrigin}
\end{equation}

\begin{equation}
  \sum_{i = 2}^{n}\left(100(x_i - x_{i - 1}^2)^2 + (1 - x_{i - 1})^2\right)
  \label{rosenbrock}
\end{equation}

\begin{equation}
  \frac{1}{4000} \sum_{i = 1}^{n}(x_i - 100)^2 - \prod_{i = 1}^{n} cos\left(\frac{x_i - 100}{\sqrt{i + 1}}\right) + 1
  \label{griewangk}
\end{equation}

\begin{equation}
  -20 e^{-0.2 \sqrt{\frac{1}{n} \sum_{i = 1}^n x_i^2}} - e^{\frac{1}{n} \sum_{i = 1}^n cos(2 \pi x_i)} + 20 + e
  \label{ackley}
\end{equation}

\begin{table}
  \centering
  \begin{tabular}{lllllll}
    \toprule
    Name & Eq. & Boundaries & Solution & Optimum & Ref. \\
    \midrule
    Sphere & (\ref{sphere}) & $\vec{x} \in {(-100, 100)}^k$ & $(0.0, \ldots, 0.0)$ & $0.0$ & f1\\
    Rastrigin & (\ref{rastrigin}) & $\vec{x} \in {(-5.12, 5.12)}^k$ & $(0.0, \ldots, 0.0)$ & $0.0$ & f2 \\
    Rosenbrock & (\ref{rosenbrock}) & $\vec{x} \in {(-10.0, 10.0)}^k$ & $(1.0, \ldots, 1.0)$ & $0.0$ & f3 \\
    Griewangk & (\ref{griewangk}) & $\vec{x} \in {(-600.0, 600.0)}^k$ & $(0.0, \ldots, 0.0)$ & $0.0$ & f4 \\
    Ackley & (\ref{ackley}) & $\vec{x} \in {(-30.0, 30.0)}^k$ & $(0.0, \ldots, 0.0)$ & $0.0$ & f5 \\
    \bottomrule
  \end{tabular}
  \label{testfunctions}
  \caption{Test Function Properties}
\end{table}

\section{Results}

\begin{table}[!htbp]
  \centering
  \begin{tabular}{ccccccc}
    \toprule
    {} & {} & f1 & f2 & f3 & f4 & f5 \\
    \midrule
    \multirow{3}{*}{Nelder-Mead} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{Luus-Jaakola} & Min & $2.23 \times 10^{-7}$ & $7.96$ & $1.32$ & $0.0246$ & $1.4 \times 10^{-5}$ \\
    & Max & $6.10 \times 10^{-6}$ & $72.42$ & $279.11$ & $0.9701$ & $3.0266$ \\
    & Med & $1.42 \times 10^{-6}$ & $25.37$ & $7.43$ & $0.1821$ & $3.7 \times 10^{-5}$ \\
    \hline
    \multirow{3}{*}{DE} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{full})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{ring})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{grid})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \bottomrule
    \hline
  \end{tabular}
  \caption{Algorithm performance when optimizing test functions in terms of lowest function value achieved.}
\end{table}

\begin{table}[!htbp]
  \centering
  \begin{tabular}{ccccccc}
    \toprule
    {} & {} & f1 & f2 & f3 & f4 & f5 \\
    \midrule
    \multirow{3}{*}{Nelder-Mead} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{Luus-Jaakola} & Min & $222$ & $58$ & $267$ & $126$ & $221$ \\
    & Max & $247$ & $243$ & $381$ & $263$ & $303$ \\
    & Med & $238.0$ & $234.0$ & $312.0$ & $254.0$ & $287.0$ \\
    \hline
    \multirow{3}{*}{DE} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{full})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{ring})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{grid})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \bottomrule
    \hline
  \end{tabular}
  \caption{Algorithm performance when optimizing test functions in terms of convergence speed counted as the number of iterations until stopping criterion is satisfied.}
\end{table}

\begin{table}[!htbp]
  \centering
  \begin{tabular}{ccccccc}
    \toprule
    {} & {} & Spec1 & Spec2 & Spec3 & Spec4 & Spec5 \\
    \midrule
    \multirow{3}{*}{Nelder-Mead} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{Luus-Jaakola} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{DE} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{full})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{ring})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{grid})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \bottomrule
    \hline
  \end{tabular}
  \caption{Algorithm performance when fitting synthetic spectra in terms of lowest fitness value achieved.}
\end{table}

\begin{table}[!htbp]
  \centering
  \begin{tabular}{ccccccc}
    \toprule
    {} & {} & Spec1 & Spec2 & Spec3 & Spec4 & Spec5 \\
    \midrule
    \multirow{3}{*}{Nelder-Mead} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{Luus-Jaakola} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{DE} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{full})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{ring})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \hline
    \multirow{3}{*}{PSO (\texttt{grid})} & Min & - & - & - & - & - \\
    & Max & - & - & - & - & - \\
    & Med & - & - & - & - & - \\
    \bottomrule
    \hline
  \end{tabular}
  \caption{Algorithm performance when fitting synthetic spectra in terms of convergence speed counted as the number of iterations until stopping criterion is satisfied.}
\end{table}

\section{Conclusions}

\bibliographystyle{abbrv}
\bibliography{bibliography}

\end{document}
