from subprocess import call
import tempfile
import os
import numpy as np

def measure_fit(real, synthetic):
    """
    Measure Chi^2
    
    Arguments:
    ----------
    real -- real spectrum
    synthetic -- synthetic spectrum, x column must be in increasing order

    Returns:
    --------
    Chi^2 error
    """
    s_y = np.interp(real[:,0], synthetic[:,0], synthetic[:,1])
    r_y = real[:,1]
    return ((r_y - s_y) ** 2 / r_y).sum()

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Measure the fit between a synthetic and real supernova spectra.')
    parser.add_argument('-i', '--input', required=True, help='input yaml file with simulation parameters')
    parser.add_argument('-r', '--real', required=True, help='real supernova spectrum')
    args = parser.parse_args()
    real = []
    for line in open(args.real, 'r'):
        real.append([float(nr.strip()) for nr in line.strip().split()])
    real = sorted(real, key=lambda row: row[0])
    real = np.array(real)
    (fd, filename) = tempfile.mkstemp()
    os.close(fd)
    os.system("tardis %s %s > /dev/null 2>&1" % (args.input, filename))
    synthetic = []
    for line in open(filename, 'r'):
        synthetic.append([float(nr.strip()) for nr in line.strip().split()])
    synthetic = sorted(synthetic, key=lambda row: row[0])
    synthetic = np.array(synthetic)
    error = measure_fit(real, synthetic)
    os.system("rm %s" % filename)
    print error
